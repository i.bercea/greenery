import sqlite3

from flask import g, app

DATABASE = 'database.db'


# This file creates the database and initializes the connection to it. Basic functions are created here to then
# simplify SQL query usage throughout the rest of the script.


# Connect to the database.
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


# Commit to the database, if content does not already exist (create structuring).
def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()


# Execute an SQL query and return any result.
def query_db(query, args=(), one=False):
    get_db().row_factory = dict_factory
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    get_db().commit()
    cur.close()
    return (rv[0] if rv else None) if one else rv


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
