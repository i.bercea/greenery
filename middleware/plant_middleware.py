from shamrock import Shamrock

from middleware import database_access
from sensors_read import relay_action

# This file provides the middleware for the plants object, which is stored in the database
# calls are also made to the third-party API trefle from here


shamrock = Shamrock('UjM0YlJKdXVFN0tNSkN2Vjd2cWo4QT09')


# Get a plant from the database and a fetch an image from the Trefle API
def plant_get(plant_id):
    query = " select * from plants where plant_id = ?"
    plant = database_access.query_db(query, [plant_id], one=True)
    try:
        plant['image'] = shamrock.species(plant['trefle_id'])['images'][0]['url']
    except:
        plant['image'] = "https://upload.wikimedia.org/wikipedia/commons/6/6e/Diversity_of_plants_image_version_5.png"
    return plant


# Get a plant from the database and all the information from the Trefle API
def plant_get_trefle_data(plant_id):
    query = " select * from plants "
    plant = database_access.query_db(query, [], one=True)
    try:
        plant['trefle'] = shamrock.species(plant['trefle_id'])
    finally:
        return plant


# Get all pants from DB
def plants_get():
    query = " select * from plants "
    plant = database_access.query_db(query, [])
    return plant

def updatePlantWatered():
    from datetime import date
    today = date.today()
    last_watered = today.strftime("%Y-%m-%d")
    update_statement = "update plants set last_watered = ? "
    database_access.query_db(update_statement, [last_watered])

# Get trefle API info from a given trefle ID
def plants_get_trefle_data(trefle_id):
    plant_data = shamrock.species(trefle_id)
    return plant_data


# Get all plants with there trefle information
def plants_get_with_trefle():
    query = " select * from plants "
    plants = database_access.query_db(query, [])
    for plant in plants:
        try:
            plant['image'] = shamrock.species(plant['trefle_id'])['images'][0]['url']
        except:
            plant[
                'image'] = "https://upload.wikimedia.org/wikipedia/commons/6/6e/Diversity_of_plants_image_version_5.png"
    return plants


# Get historical data on a given plant
def get_plant_history():
    query = "select * from historic_data  asc limit 60 "
    res = database_access.query_db(query)
    return res


# Add historical data for a given plant
def plant_add_history(temp, moisture, light, fire):
    import time
    moment = int(time.time())
    query = "insert into historic_data ( light_perc, fire_perc, moisture_perc, temp_perc, time) " \
            "values(?,?,?,?,?) "
    entry = database_access.query_db(query,
                                     [light, fire, moisture, temp, moment],
                                     one=True)
    return entry


# Add a plant to the system
def plant_add(human_name, species, trefle_id, last_watered, days_between_watering, location):
    if last_watered is None or last_watered.strip() == "":
        from datetime import date
        today = date.today()
        last_watered = today.strftime("%Y-%m-%d")
    query = "insert into plants (human_name, species, trefle_id, last_watered, days_between_watering, location) " \
            "values(?,?,?,?,?,?) "
    plant = database_access.query_db(query,
                                     [human_name, species, trefle_id, last_watered, days_between_watering, location],
                                     one=True)
    return plant


# Update a stored plants information
def plant_update(plant_id, human_name, species, trefle_id, last_watered, days_between_watering, location):
    query = "update plants  set human_name = ?, species = ?, trefle_id = ?, last_watered = ?, days_between_watering = " \
            " ?, location = ? where plant_id = ? "
    plant = database_access.query_db(query,
                                     [human_name, species, trefle_id, last_watered, days_between_watering, location,
                                      plant_id], one=True)
    return plant


# Remove a Stored Plant
def plant_remove(plant_id):
    query = " delete from plants where plant_id = ?"
    plant = database_access.query_db(query, [plant_id], one=True)
    return plant


# Water all plants if they need it depending on when they where last watered
def water_plants():
    query = "select plant_id from plants p  where julianday('now') - julianday(last_watered) > " \
            "p.days_between_watering "
    # Currently only supports one plant
    print("Checking Plants to Water")
    if len(database_access.query_db(query)) > 0:
        plant_id = database_access.query_db(query)[0]['plant_id']
        print("Watering plant: " + str(plant_id))
        watered = relay_action()
        from datetime import date
        today = date.today()
        last_watered = today.strftime("%Y-%m-%d")
        update_statement = "update plants set last_watered = ? "
        database_access.query_db(update_statement, [last_watered])
        return True
    return False


def plant_count():
    query = "select count(*) from plants"
    return database_access.query_db(query,[])[0]['count(*)']
