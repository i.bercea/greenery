import sys

from flask import Blueprint
from flask import jsonify, request, abort
from shamrock import Shamrock

from sensors_read import relay_action

# This library functions as a routing for plant information. So everything that is plant related will be handled
# under this library.

# This setups the API that allows connection to Trefle. Trefle is the API we use to gather some information regarding
# the plant.

shamrock = Shamrock('UjM0YlJKdXVFN0tNSkN2Vjd2cWo4QT09')

from middleware import plant_middleware

sys.path.insert(1, '/middleware')
plants_router = Blueprint('plants_router', __name__)


# Route a GET request of / (homepage), to return all the plants that are currently stored in the database.
@plants_router.route('/', methods=['GET'])
def get_plants():
    return jsonify({'plants': plant_middleware.plant_get(1)})

# Whenever we get a GET request for a specific plant ID (or mostly, an integer passed), we return the data for that
# plant ID, along with the Trefle information (API)
@plants_router.route('/<int:plant_id>', methods=['GET'])
def get_plant(plant_id):
    plants = plant_middleware.plant_get(1)
    plant = [plant for plant in plants if plant['id'] == plant_id]
    trefle_id = shamrock.species(plant['trefle_id'])
    if len(plant) == 0:
        abort(404)
    response = {
        'plant': plant_middleware.plant_get(plant_id),
        'trefle': plant_middleware.plants_get_trefle_data(trefle_id)
    }
    return jsonify(response)

# POST requests under main directory (whenever a new plant is created), are handled under this route, with the plant
# then being added into the database.
@plants_router.route('/', methods=['POST'])
def create_plant():
    if not request.json:
        abort(400)
    plant = request.json.get("plant")
    species_id = shamrock.search(plant['species'])[0]["id"]
    added_plant = plant_middleware.plant_add(plant['human_name'], plant['species'], species_id, plant['last_watered'],
                                             plant['days_between_watering'], plant['location'])
    return jsonify({'plant': added_plant}), 201

# Whenever the plant is updated (from the plant page, when data is changed), then we send an update request to the
# database.
@plants_router.route('/', methods=['PUT'])
def update_plant():
    if not request.json:
        abort(400)
    plant = request.json.get("plant")
    try:
        plant['species_id'] = shamrock.search(plant['species'])[0]["id"]
    except Exception:
        plant['species_id'] = 0
    updated_plant = plant_middleware.plant_update(plant['plant_id'], plant['human_name'],
                                                  plant['species'], plant['species_id'], plant['last_watered'],
                                                  plant['days_between_watering'], plant['location'])
    return jsonify({'plant': updated_plant}), 201

# Sets up the routing required when we want to delete a plant from the database.
@plants_router.route('/<int:plant_id>', methods=['DELETE'])
def delete_plant(plant_id):
    if plant_id is None:
        abort(404)
    plant_middleware.plant_remove(plant_id)
    return jsonify({'result': True})

# When there is a watering request for a plant, we activate the relay and get a True boolean response. If the plant
# does not exist, then a 404 error will be called.
@plants_router.route('/water/<int:plant_id>', methods=['GET'])
def water(plant_id):
    if plant_id is None:
        abort(404)
    relay_action()
    plant_middleware.updatePlantWatered()
    return jsonify({'result': True})

# Returns the historic data for a specific plant ID (data stored in the database for each individual plant).
@plants_router.route('/historic/<int:plant_id>', methods=['GET'])
def historic_data(plant_id):
    return jsonify({'data': plant_middleware.get_plant_history()})
    # if plant_id is None:
    #     abort(404)
    # relay_action()
    # return jsonify({'result': True})

# Returns the historic data for a specific plant ID (data stored in the database for each individual plant).
@plants_router.route('/count/', methods=['GET'])
def count():
    return jsonify({'count': plant_middleware.plant_count()})





