import sys
import threading
import time
from datetime import datetime
from random import randint

import schedule

sys.path.insert(1, '/home/pi/webapp/project-greenery')
from sensors_read import data_array, relay_action

sys.path.insert(1, '/usr/lib/python3.7/site-packages')
# This library serves as the routing page for the main page, as well as organizes the scheduling for checking whether
# the plant has to be watered.

sys.path.insert(1, '/routes')
sys.path.insert(1, '/middleware')
from middleware import plant_middleware

from flask import Flask, jsonify, make_response, render_template, \
    copy_current_request_context
from routes.plants_router import plants_router
from flask_socketio import SocketIO, emit

TEMPLATES_AUTO_RELOAD = True
app = Flask(__name__)
app.register_blueprint(plants_router, url_prefix='/plants')

# declare socketio (to then be initialized)
socketio = SocketIO(app, monitor_client=True)

watering_time = "12:12"


# initializes the thread that will work on checking whether the plant needs watering.
# this also passes the data to the front-end through the "data" array.
# Data consists of: temperature, light, fire (1/0), moisture.
# This data is stored in the database to then be used for historic purposes.

def start_watering_thread():
    schedule.every().day.at(watering_time).do(plant_middleware.water_plants)
    schedule.every(20).seconds.do(plant_middleware.water_plants)

    @copy_current_request_context
    def schedule_thread():
        while (True):
            schedule.run_pending()
            data = data_array()
            plant_middleware.plant_add_history(data[4], data[0], data[3], data[2])
            time.sleep(10)

    schedule_thread_out = threading.Thread(target=schedule_thread)
    schedule_thread_out.start()


app.before_first_request(start_watering_thread)

if __name__ == '__main__':
    socketio.run(app)

thread = "global"


# Initialize socket and test connection; send a set of data and initialize thread to only work on sending such data.
@socketio.on('connect')
def test_connect():
    @copy_current_request_context
    def thread_function():
        while (True):
            data = data_array()
            test_message(data)
            plant_middleware.plant_add_history(data[4], data[0], data[3], data[2],)
            if data[2] == 1:
                relay_action()
            time.sleep(1)

    read_out_thread = threading.Thread(target=thread_function)
    read_out_thread.start()


def test_message(message):
    emit('plant_update', {'data': message})


# Route / to index.html (main page)
@app.route("/")
def bundle():
    plants = plant_middleware.plants_get_with_trefle()
    return render_template("index.html", plants=plants)


# Route /create to create.html (plant creation/registration page)
@app.route("/create")
def create():
    plants = plant_middleware.plants_get_with_trefle()
    print(plants)
    return render_template("create.html", plants=plants)


# Route /plant/<id> to the information page for that specific plant ID.
@app.route("/plant/<int:plant_id>")
def plant(plant_id):
    plant = plant_middleware.plant_get_trefle_data(plant_id)
    return render_template("plant.html", plant=plant)


app.errorhandler(404)


# Respond with a json format to any 404 errors
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)
