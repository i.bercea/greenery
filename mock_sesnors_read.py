import time
from random import randint


# This file is only for development purposes. Basically, it would send data to the front-end to see whether the
# socket was set up correctly and if the graphs (historic and live) would properly display information sent to it.

def relay_action(plant_id):
    print("Turning on pump if required")
    time.sleep(5)  # pump on for 5s
    print("Turning off Pump")
    return True


def manual_relay_action(timer):
    print("Manual Pump turn on")
    time.sleep(timer)  # pump on for timer specified
    print("Manual Pump turn off")


# Mock data, consisting of random integers from 0 to 100
def data_array():
    return [randint(0, 100) for p in range(0, 5)]
