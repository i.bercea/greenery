CREATE TABLE `users`
(
    `userId`   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `email`    TEXT    NOT NULL UNIQUE,
    `password` TEXT    NOT NULL
);


CREATE TABLE `plants`
(
    `plantId`             INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `human-name`          TEXT UNIQUE,
    `trefleId`            INTEGER,
    `lastWatered`         INTEGER NOT NULL,
    `daysBetweenWatering` INTEGER NOT NULL DEFAULT 0
)