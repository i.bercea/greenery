# import needed modules
import glob
import time
from time import sleep
from numpy import interp

import RPi.GPIO as GPIO
from Adafruit_ADS1x15 import ADS1x15

# GPIO numbering
GPIO.setmode(GPIO.BCM)

# avoid console warnings for already set pins, they are overwritten
GPIO.setwarnings(False)

# GPIO pins connected
fire_pin = 14
temperature_pin = 4
distance_pin_echo = 24
distance_pin_trigger = 23
relay_pin = 10

# set up GPIO pins
GPIO.setup(fire_pin, GPIO.IN, pull_up_down=GPIO.PUD_OFF)
GPIO.setup(temperature_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(distance_pin_echo, GPIO.IN)
GPIO.setup(distance_pin_trigger, GPIO.OUT)
GPIO.setup(relay_pin, GPIO.OUT)

# make sure relay are closed at first
GPIO.output(relay_pin, False)

# assigning the ADS1x15 ADC
ADS1015 = 0x00  # 12-bit ADC
ADS1115 = 0x01  # 16-bit

gain = 4096  # +/- 4.096V
sps = 64  # 64 Samples per second

# assigning the ADC-Channel
adc_channel_0 = 0  # Channel 0
adc_channel_1 = 1  # Channel 1

# initialise ADC (ADS1115)
adc = ADS1x15(ic=ADS1015)

# starting communication with the temperature sensor
base_dir = '/sys/bus/w1/devices/'
while True:
    try:
        device_folder = glob.glob(base_dir + '28*')[0]
        break
    except IndexError:
        sleep(0.5)
        continue
device_file = device_folder + '/w1_slave'


# current measures at temperature sensor
def temp_measure():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines


# temperature read in Celsius degrees
def temp_analysis():
    lines = temp_measure()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = temp_measure()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos + 2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c


# assigning '1' or '0' according to read status of pin
def digital_read(pin):
    if GPIO.input(pin) == False:
        return 0  # no fire/light
    else:
        return 1  # fire/light


# computes distance in centimeters to an object using ultrasonic waves
def distance():
    GPIO.output(distance_pin_trigger, True)  # set trigger to high
    time.sleep(0.00001)  # after 0.01ms
    GPIO.output(distance_pin_trigger, False)  # set trigger to low
    StartTime = time.time()
    StopTime = time.time()
    while GPIO.input(distance_pin_echo) == 0:
        StartTime = time.time()  # save start time
    while GPIO.input(distance_pin_echo) == 1:
        StopTime = time.time()  # save time of arrival
    TimeElapsed = StopTime - StartTime
    distance = (TimeElapsed * 34300) / 2
    return distance


# relay turning pump on/off
def relay_action():
    GPIO.output(relay_pin, True)  # relay switch on
    time.sleep(5)  # pump on for 5s
    GPIO.output(relay_pin, False)  # relay switch off


# method called by web app to fetch sensor reads
def data_array():
    temp = int(temp_analysis())
    fire = digital_read(fire_pin)
    adc0 = adc.readADCSingleEnded(adc_channel_0, gain, sps)
    adc1 = adc.readADCSingleEnded(adc_channel_1, gain, sps)
    soil = interp(adc0, [400, 3300], [0, 100])
    light = interp(adc1, [50, 1000], [100, 0])
    #print("distance = ", dist)
    dist = interp(distance(), [3, 6], [100, 0])
    # print("soil = ", soil, "adc0 = ", adc0)
    # print("light = ", light, "adc1 = ", adc1)
    #print("dist = ", dist)
    #print("fire = ", fire)
    # print("temp = ", temp)
    data = [soil, dist, fire, light, temp]
    return data
